/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;

/**
 * Simple SSL chat client modified from {@link //TelnetClient}.
 */
public class SecureChatClient {

    static final String HOST = System.getProperty("host", "127.0.0.1");
    static final int PORT = Integer.parseInt(System.getProperty("port", "8992"));
    public static boolean powerOn = true;

    public static void main(String[] args) throws Exception {


        final SslContext sslCtx = SslContextBuilder.forClient()
                .trustManager(InsecureTrustManagerFactory.INSTANCE).build();

        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new SecureChatClientInitializer(sslCtx));


            // Start the connection attempt.
            Channel ch = b.connect(HOST, PORT).sync().channel();

            ChannelFuture lastWriteFuture = null;
            // lastWriteFuture = ch.writeAndFlush("" + "\r\n");
            MainDataClass mainDataClass = new MainDataClass();
            mainDataClass.CheckKeys();

            JsonObject rootObject = new JsonObject(); // создаем главный объект
            rootObject.addProperty("type", "getServerPublicKey"); // записываем текст в поле "message"
            Gson gson = new Gson();
            ch.writeAndFlush(gson.toJson(rootObject));      //TODO Сервер не получает это сообщение
            System.out.println(gson.toJson(rootObject));

           /* JsonObject childObject = new JsonObject(); // создаем объект Place



            lastWriteFuture = ch.writeAndFlush(json);
            // Read commands from the stdin.
        /*    ChannelFuture lastWriteFuture = null;
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            for (;;) {
                String line = in.readLine();
                if (line == null) {
                    break;
                }
                // Sends the received line to the server.
                lastWriteFuture = ch.writeAndFlush(line + "\r\n");

                // If user typed the 'bye' command, wait until the server closes
                // the connection.
                if ("bye".equals(line.toLowerCase())) {
                    ch.closeFuture().sync();
                    break;
                }
            }*/


            while (powerOn) {
                ch.writeAndFlush(gson.toJson(rootObject)).sync(); //TODO Убрать
                SecureChatClient scc = new SecureChatClient();
                scc.GetMsg(ch);
            }


        } finally {
            // The connection is closed automatically on shutdown.
            group.shutdownGracefully();
        }
    }

    public void GetMsg(Channel ch) throws Exception {       //TODO А нужен ли этот метод?
        BaseUI baseUI = new BaseUI();
        ch.writeAndFlush(baseUI.enterText());
        System.out.println("Succefull send!");
    }

}