import java.util.Scanner;

public class BaseUI {

    public String enterText() throws Exception
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter name your friend: ");
        String toId = in.next();
        System.out.println("Enter next:");
        String msg = in.next();
        MainDataClass mainDataClass = new MainDataClass();
        AesCrypt aesCrypt = new AesCrypt();
        PassGen passGen = new PassGen();
        String iv = passGen.PassGenMain(true, true, false, 16);
        return mainDataClass.SetMessage("msg", toId, iv, aesCrypt.Encrypt(mainDataClass.DecryptKey(),iv, msg));
    }
}
