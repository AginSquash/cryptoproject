import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.io.IOException;
import java.nio.file.Files;

public class MainDataClass {

   PassGen passGen = new PassGen();

    public void CheckKeys() throws Exception
    {
        if((!new File("KeyPair/privateKey").exists())||(!new File("KeyPair/publicKey").exists()))
        {
            System.err.println("Keys not found! Generating...");
            MainDataClass mainDataClass = new MainDataClass();
            mainDataClass.GenerateKeys();
            mainDataClass.EncryptKey();
            System.err.println("Succefull!");
        }
    }

    public void EncryptKey() throws Exception      //it will be gen new AES-256 key
    {
        AsymmetricCryptography ac = new AsymmetricCryptography();
        PublicKey publicKey = ac.getPublic("KeyPair/publicKey");

        ac.encryptKeyFile(passGen.PassGenMain(true, true, false, 32).getBytes(), new File("KeyPair/AES"), publicKey);

    }

    public String DecryptKey() throws Exception  {
        AsymmetricCryptography ac = new AsymmetricCryptography();
        PrivateKey privateKey = ac.getPrivate("KeyPair/privateKey");
        return new String(ac.decryptKey(ac.getFileInBytes(new File("KeyPair/AES")), privateKey), Charset.defaultCharset());
    }

    public void GenerateKeys() throws Exception
    {
        GenerateKey generateKey = new GenerateKey(2048);
        generateKey.main(2048);
    }


    public String GetToken() throws Exception{      //TODO Токен нужно получить от сервера

        MainDataClass mainDataClass = new MainDataClass();
        return new String( mainDataClass.getFileInBytes(new File("Data/token")), Charset.defaultCharset());
    }


    public String SetMessage(String type, String toId, String IV, String msgEnterd) {
        JsonObject rootObject = new JsonObject(); // создаем главный объект
        rootObject.addProperty("type", type);
        rootObject.addProperty("toId", toId); // записываем текст в поле "message"
        rootObject.addProperty("IV", IV);
        rootObject.addProperty("msg", msgEnterd);
        Gson gson = new Gson();
        return gson.toJson(rootObject); // генерация json строки
    }

    public byte[] getFileInBytes(File f) throws IOException {
        FileInputStream fis = new FileInputStream(f);
        byte[] fbytes = new byte[(int) f.length()];
        fis.read(fbytes);
        fis.close();
        return fbytes;
    }
}
